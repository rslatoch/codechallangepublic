using Microsoft.VisualStudio.TestTools.UnitTesting;
using Security.Validation;
namespace Test.Domain
{
    [TestClass]
    public class MatrixTests
    {
        [TestMethod]
        public void MatrixNonSquare()
        {
            string ret;
            Matrix.Validate("1,2,3,4,5", out ret);
            Assert.AreEqual(ret, "Invalid matrix, must be a square one!");
        }
        [TestMethod]
        public void MatrixSquare()
        {
            string ret;
            Matrix.Validate("1,2,3,4", out ret);
            Assert.AreEqual( ret, "Invalid matrix, must be a triangular one!");
        }
        [TestMethod]
        public void MatrixSquareUpperTriangular()
        {
            Assert.AreEqual(true, Matrix.Validate("1,0,3,4"));
        }
        [TestMethod]
        public void MatrixSquareLowerTriangular()
        {
            Assert.AreEqual(true, Matrix.Validate("1,2,0,4"));
        }
        [TestMethod]
        public void MatrixSquareLowerTriangular3x3()
        {
            Assert.AreEqual(true, Matrix.Validate("1,2,3,0,5,6,0,0,9"));
        }
        [TestMethod]
        public void MatrixSquareUpperTriangular3x3()
        {
            Assert.AreEqual(true, Matrix.Validate("1,0,0,4,5,0,7,8,9"));
        }
        [TestMethod]
        public void MatrixSquareNonLTriangular3x3()
        {
            Assert.AreEqual(false, Matrix.Validate("1,2,3,0,5,6,0,1,9"));
        }
        [TestMethod]
        public void MatrixSquareNonUTriangular3x3()
        {
            Assert.AreEqual(false, Matrix.Validate("1,0,1,4,5,0,7,8,9"));
        }
        [TestMethod]
        public void MatrixSquareLowerTriangular4x4()
        {
            Assert.AreEqual(true, Matrix.Validate("1, 2 ,3 ,4 ,0 ,6, 7, 8, 0 ,0, 11 ,12, 0, 0, 0 ,16"));
        }
        [TestMethod]
        public void MatrixSquareUpperTriangular4x4()
        {
            Assert.AreEqual(true, Matrix.Validate("1 ,0, 0, 0, 5, 6 ,0, 0, 9 ,10 ,11, 0 ,13 ,14 ,15, 16"));
        }
        [TestMethod]
        public void MatrixSquareNonLTriangular4x4()
        {
            Assert.AreEqual(false, Matrix.Validate("1,2 ,3 ,4 ,0 ,6, 7, 8, 2 ,0, 11 ,12, 0, 0, 0 ,16"));
        }
        [TestMethod]
        public void MatrixSquareNonUTriangular4x4()
        {
            Assert.AreEqual(false, Matrix.Validate("1 ,0, 0, 1, 5, 6 ,0, 0, 9 ,10 ,11, 0 ,13 ,14 ,15, 16"));
        }
    }
}
