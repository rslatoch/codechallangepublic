using Microsoft.VisualStudio.TestTools.UnitTesting;
using Data.Domain.Models;
using System.Linq;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore; 
using Service.Domain.Services;
using System;
using Data.Domain.Supervisor;
using System.Collections.Generic;

namespace Test.Domain
{
    [TestClass]
    public class CRUDTests
    {
        private AppDbContext _ctx;
        private UserService _usr;
        private User _LocalUser;
        private UserSupervisor _usrSuper;

        public CRUDTests()
        {  
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>()
                .UseNpgsql("Host=xxxxx")
                .Options;
            _ctx = new AppDbContext(optionsBuilder);
            _usr = new UserService(this._ctx);
            _usrSuper = new UserSupervisor(this._ctx);
        }

         
        [TestInitialize]
        public void Create()
        {  
            User _tmpUser = new User
            {
                Birthday = DateTime.Now,
                Email = "sometestes-0123456789@testeFix.com",
                Notes = "a regular lorem ipsum",
                Name = "Joao Jose Gilberto",
                Illuminati = true               
            };
           List<User> _lstUser = _usrSuper.Find("", _tmpUser.Email);
            if (_lstUser.Count > 0)
                _usr.Remove(_lstUser[0].UserId);
            _usr.Add(_tmpUser);
            _LocalUser = _usrSuper.Find("", _tmpUser.Email)[0];
            Assert.AreNotEqual(_LocalUser.UserId, 0 );
        }
        [TestMethod]
        public void Illuminati()
        {
           // _LocalUser = _usrSuper.Find(_LocalUser.UserId);
            _usr.AlterFlagIlluminati(_LocalUser.UserId, !_LocalUser.Illuminati);
            Assert.AreNotEqual(_usrSuper.Find(_LocalUser.UserId).Illuminati, _LocalUser.Illuminati);
        }

        [TestCleanup]
        public void Delete()
        { 

            Assert.AreEqual(true,_usr.Remove(_LocalUser.UserId));
        }
    }
}