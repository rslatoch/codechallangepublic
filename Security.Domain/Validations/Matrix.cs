﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Security.Validation
{
    public class Matrix
    {
        public static bool Validate(string matrix)
        {
            string ret;
            return Validate(matrix, out ret);
        }
        public static bool Validate(string matrix, out string mensage)
        {
            try
            {
                string[] _arrMatrix = matrix.Split(',');
                int _sqrVal = (int)Math.Sqrt(_arrMatrix.Count());
                bool _mtrxUpper = true;
                bool _mtrxLower = true;

                #region Basic_Validation
                //first basic validations for less operations and processor stress 
                if (_sqrVal <= 1 || _sqrVal != Math.Sqrt(_arrMatrix.Count()))
                {
                    //Normaly in validation methods I put a generic return (without the reason for the user)
                    mensage = "Invalid matrix, must be a square one!";
                    return false;
                }

                if (!(_arrMatrix[_sqrVal - 1].Trim().Equals("0") || _arrMatrix[(_sqrVal - 1) * _sqrVal].Trim().Equals("0")))
                {
                    //Normaly in validation methods I put a generic return (without the reason for the user)
                    mensage = "Invalid matrix, must be a triangular one!";
                    return false;
                }
                #endregion

                #region upper_Validation
                for (int _ln = 0; _ln < _sqrVal - 1; _ln++)
                    for (int _cl = _ln + 1; _cl < _sqrVal; _cl++)
                        if (!_arrMatrix[_sqrVal * _ln + _cl].Trim().Equals("0"))
                        {
                            _mtrxUpper = false;
                            _ln = _sqrVal;
                            _cl = _ln;
                        }
                            
                #endregion

                #region lower_Validation
                for (int _ln = 1; _ln < _sqrVal; _ln++)
                    for (int _cl = 0; _cl < _ln ; _cl++)
                        if (!_arrMatrix[(_sqrVal * _ln) + _cl].Trim().Equals("0"))
                        {
                            _mtrxLower = false;
                            _ln = _sqrVal;
                            _cl = _ln;
                        }
                #endregion 

                if (_mtrxLower || _mtrxUpper)
                {
                    mensage = "Valid matrix";
                    return true;
                }
                //Normaly in validation methods I put a generic return (without the reason for the user)
                mensage = "Invalid matrix, must be a triangular one!";
                return false;
            }
            catch (Exception ex)
            {
                mensage = ex.Message;
                return false;
            }

        }
    }
}
