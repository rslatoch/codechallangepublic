﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 
using Microsoft.AspNetCore.Mvc;

namespace API.Domain.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatrixController : ControllerBase
    {
        // GET api/Matrix/1,2,0,4
        [HttpGet("{matrix}")]
        public ActionResult<string> Get(string matrix)
        {
            try
            {
                string ret;
                if (Security.Validation.Matrix.Validate(matrix, out ret)) 
                    return Ok(ret); 
                return BadRequest(ret);
            }
            catch (Exception ex)
            { 
                return BadRequest(ex.Message);
            } 
        } 
    }
}
