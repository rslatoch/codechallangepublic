﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Domain.Models;
using Data.Domain.Views;
using Data.Domain.Views.UserReturn;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Service.Domain.Services;

namespace API.Domain.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _usrServ;

        public UserController(AppDbContext appDb)
        {
            _usrServ = new UserService(appDb);
        }

        [HttpGet("{id}")]
        public ActionResult<ViewPerson> GetById([FromRoute] int id)
        {
            try
            {
                ViewPerson _usr = _usrServ.GetById(id);
                if (_usr is null)
                    return NotFound();
                return Ok(_usr);
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error during load user{0}{1}", Environment.NewLine, e.Message));
            }

        }
        [HttpGet("GetUserWhitoutFormat/{id}")]
        public ActionResult<string> GetByIdWhithoutFormat([FromRoute] int id)
        {
            try
            {
                User _usr = _usrServ.GetByIdWhithoutFormat(id);
                if (_usr is null)
                    return NotFound();
                return Ok(Newtonsoft.Json.JsonConvert.SerializeObject(_usr));
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error during load user{0}{1}", Environment.NewLine, e.Message));
            }

        }
        [HttpGet("GetByName/{name}")]
        public ActionResult<ViewPeople> GetByName([FromRoute] string name)
        {
            try
            {
                ViewPeople _usr = _usrServ.GetByName(name);
                if (_usr is null)
                    return NotFound();
                return Ok(_usr);
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error during load user{0}{1}", Environment.NewLine, e.Message));
            }

        }
        [HttpGet]
        public ActionResult<ViewPeople> GetUsers([FromQuery] int? page = 1, [FromQuery] int? offset = 10)
        {
            try
            {
                page -= 1;
                if (page < 0)
                    page = 0;
                ViewPagination _pag = new ViewPagination();
                _pag.Page = (int)page;
                _pag.OffSet = (int)offset;
                ViewPeople _ret = _usrServ.GetList(ref _pag);
                _pag.Page += 1;
                Response.Headers.Add("X-Total", _pag.Count.ToString());
                Response.Headers.Add("X-Per-Page", _pag.OffSet.ToString());
                Response.Headers.Add("X-Page", _pag.Page.ToString());
                Response.Headers.Add("X-Total-Pages", _pag.TotalPages().ToString());
                return Ok(_ret);
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error during load list{0}{1}", Environment.NewLine, e.Message));
            }
        }
        [HttpPost]
        public ActionResult AddUsr(ViewUserAdd user)
        {
            try
            {
                if (_usrServ.Add(user)>0)
                    return Ok();
                return BadRequest("Error during adding user");
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error during adding user{0}{1}",Environment.NewLine,e.Message));
            }
        }
        [HttpGet("topFiveViews")]
        public ActionResult<List<ViewReturnTopFive>> topFiveViews()
        {
            try
            {
                List<ViewReturnTopFive> _lst = _usrServ.TopFive();
                if (_lst.Count() > 0)
                    return Ok(_lst);
                return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error during loading list{0}{1}", Environment.NewLine, e.Message));
            }
        }
        [HttpGet("topFiveViews/{month}/{year}")]
        public ActionResult<List<ViewReturnTopFive>> topFiveViews([FromRoute] int month, [FromRoute] int year)
        {
            try
            {
                List<ViewReturnTopFive> _lst = _usrServ.TopFive(month, year);
                if (_lst.Count() > 0)
                    return Ok(_lst);
                return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error during load list{0}{1}", Environment.NewLine, e.Message));
            }
        }
        [HttpGet("sendInvite/{email}/{newUserName}")]
        public ActionResult<string> topFiveViews([FromRoute] string email, [FromRoute] string newUserName)
        {
            try
            {
                StringValues usrId;
                Request.Headers.TryGetValue("JWTuser", out usrId);
                _usrServ.SendInvite(int.Parse(usrId), email, newUserName);
                return Ok("Invitation sent!"); 
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("Error sending e-mail{0}{1}", Environment.NewLine, e.Message));
            }
        }

    }
}