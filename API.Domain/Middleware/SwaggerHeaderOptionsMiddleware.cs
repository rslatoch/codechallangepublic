﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace API.Domain.Middleware
{ 
    //this is not a "real" middleware, but I need to put this someware to organize
    public class SwaggerHeaderOptionsMiddleware : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "JWTuser",
                In = "header",
                Type = "int",
                Required = false
            });
            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "itanimulli",
                In = "header",
                Type = "string",
                Required = false
            });
        }
    }
}