﻿using Data.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Service.Domain.Services;
using System.Threading.Tasks;

namespace API.Domain.Middleware
{
    public class IlluminatiMiddleware
    {
        RequestDelegate _next;
        private UserService _usrServ;

        public IlluminatiMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, AppDbContext appDb)
        {
            _usrServ = new UserService(appDb);
            StringValues user;
            StringValues matrix;
            bool error = true;
            int userid;
            string tmp;

            //await _next(context); 
            if (context.Request.Path.Value.Contains("/Matrix/") || context.Request.Path.Value.Contains("/swagger/"))
                await _next(context);
            else
            { 
                if (context.Request.Headers.TryGetValue("JWTuser", out user))
                    if (int.TryParse(user, out userid))
                        if (_usrServ.Exists(userid))
                        {
                            error = false;
                            if (context.Request.Headers.TryGetValue("itanimulli", out matrix))
                                _usrServ.AlterFlagIlluminati(userid, Security.Validation.Matrix.Validate(matrix, out tmp));
                            else
                                _usrServ.AlterFlagIlluminati(userid, false);
                        }
                        
                if (error)
                {
                    context.Response.StatusCode = 401;
                    await context.Response.WriteAsync("");
                }
                await _next(context); 
            }
        } 
    }
}