﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Views
{
    public class ViewUserAdd
    { 
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
    }
}
