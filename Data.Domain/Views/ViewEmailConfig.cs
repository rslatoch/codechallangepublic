﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Views
{ 
    public class EmailConfig
    {
        public string Servidor { get; set; }
        public int Porta { get; set; }
        public string Remetente { get; set; }
        public string Senha { get; set; }
        public bool SSL { get; set; }
    }
}
