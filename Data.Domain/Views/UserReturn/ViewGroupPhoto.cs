﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Views.UserReturn
{
    public class ViewPhotoMetrics
    {
        public int clicks { get; set; }
        public int shares { get; set; }
        public int likes { get; set; }
    }
    public class ViewPhotoDetail
    {
        public string URL { get; set; }
        public ViewPhotoMetrics Metrics { get; set; }
    }
    public class ViewPhoto {
        public ViewPhotoDetail Photo { get; set; }
    }   
}
