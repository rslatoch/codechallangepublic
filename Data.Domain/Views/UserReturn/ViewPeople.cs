﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Views.UserReturn
{ 
    public class ViewPeople
    {
        public List<ViewPerson> People { get; set; }
    }
}
