﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Views.UserReturn
{

    public class ViewPerson
    {
        public ViewSummaryMetrics summary_metrics { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Notes { get; set; }
    }

}
