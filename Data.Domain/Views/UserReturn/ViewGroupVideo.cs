﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Views.UserReturn
{
    public class ViewVideoMetrics
    {
        public int clicks { get; set; }
        public int completed_view { get; set; }
        public int likes { get; set; }
    }
    public class ViewVideoDetail
    {
        public string URL { get; set; }
        public ViewVideoMetrics Metrics { get; set; }
    }
    public class ViewVideo
    {
        public ViewVideoDetail Video { get; set; }
    }
}
