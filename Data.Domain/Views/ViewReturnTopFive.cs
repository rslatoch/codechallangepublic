﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Views
{
    public class ViewReturnTopFive
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public int TotalOfViews { get; set; } 
     }
}
