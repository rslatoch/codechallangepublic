﻿using Data.Domain.Models;
using Data.Domain.Views;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.Domain.Supervisor
{
    public class UserSupervisor
    {
        private AppDbContext _ctx;
        public UserSupervisor(AppDbContext appDBContext)
        {
            _ctx = appDBContext;
        }

        public List<User> toList(ref ViewPagination pag)
        {
            pag.Count = _ctx.Users.Count();
            return _ctx.Users.Include(p=>p.Posts).ThenInclude(m=>m.Metric).Skip(pag.StartAt()).Take(pag.OffSet).AsNoTracking().ToList();
        }
        public List<User> Find(string user, string email="")
        {
            return _ctx.Users.Include(p => p.Posts).ThenInclude(m => m.Metric).Where(w =>( EF.Functions.Like(w.Name.ToLower().Trim(), string.Format("%{0}%", user.ToLower().Trim())) && user.Trim() != "") || (EF.Functions.Like(w.Email.ToLower().Trim(), string.Format("%{0}%", email.ToLower().Trim())) && email.Trim() != "")).AsNoTracking().ToList();
        }
        public User Find(int Id)
        {
            return _ctx.Users.Include(p => p.Posts).ThenInclude(m => m.Metric).Where(u => u.UserId == Id).FirstOrDefault();
        }
        public bool Remove(int Id)
        {
            _ctx.Remove(_ctx.Users.Include(p => p.Posts).ThenInclude(m => m.Metric).Where(u => u.UserId == Id).FirstOrDefault());
            return _ctx.SaveChanges() != 0;
        }
        public int Add(ViewUserAdd usr)
        {
            User luser = new User { Email = usr.Email, Birthday = usr.Birthday, Name = usr.Name, Notes = usr.Notes };
            _ctx.Add(luser);
            return _ctx.SaveChanges() ;
        }
        public int Add(User usr)
        {
            _ctx.Add(usr);
            return _ctx.SaveChanges() ;
        }

        public void AlterFlagIlluminati(int userId, bool illuminati)
        {
            User tusr = Find(userId);
            tusr.Illuminati = illuminati;
            _ctx.Update(tusr);
            _ctx.SaveChanges();
        }
        public List<ViewReturnTopFive> topFive(int month, int year)
        {
            string _date = year.ToString() + month.ToString().PadLeft(2,'0');
            int[] _ids = _ctx.DisplayCounters.Where(q => q.Date.Equals(_date)).OrderByDescending(o => o.Count).Select(p => p.UserFk).Take(5).ToArray();
            //return _ctx.Users.Include(p => p.Posts).ThenInclude(m => m.Metric).Where(w => _ids.Contains(w.UserId)).ToList();
            return
                (from dc in _ctx.DisplayCounters
                join user in _ctx.Users on dc.UserFk equals user.UserId
                where _ids.Contains(dc.UserFk) && _date.Equals(dc.Date)
                orderby dc.Count descending
                select new ViewReturnTopFive { UserID = user.UserId, Name = user.Name, Birthday = user.Birthday, TotalOfViews = dc.Count }).ToList();

        } 
        public void AddView(int userid)
        {
            DisplayCounter _dc = _ctx.DisplayCounters.Where(u => u.UserFk == userid && u.Date.Equals(DateTime.Now.ToString("yyyyMM"))).FirstOrDefault();
            if (_dc is null)
            {
                _dc = new DisplayCounter { Count = 1, Date = DateTime.Now.ToString("yyyyMM"), UserFk = userid };
                _ctx.Add(_dc);
            }
            else
            {
                _dc.Count += 1;
                _ctx.Update(_dc);
            }
            _ctx.SaveChanges();
        }
        public void AddView(int userid, int month,int year)
        {
            string _date = year.ToString() + month.ToString();
            DisplayCounter _dc = _ctx.DisplayCounters.Where(u => u.UserFk == userid && u.Date.Equals(_date)).FirstOrDefault();
            if (_dc is null)
            {
                _dc = new DisplayCounter { Count = 1, Date = _date, UserFk = userid };
                _ctx.Add(_dc);
            }
            else
            {
                _dc.Count += 1;
                _ctx.Update(_dc);
            }
            _ctx.SaveChanges();
        }
    }
}
