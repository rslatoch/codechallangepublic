﻿using Data.Domain.Models;
using Data.Domain.Views.UserReturn;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Domain.Supervisor
{
    public class PostSupervisor
    {
        private AppDbContext _ctx;
        public PostSupervisor(AppDbContext appDBContext)
        {
            _ctx = appDBContext;
        }

        public bool addPhoto(int usr, ViewPhotoDetail photo)
        {
            string _ret;
            return addPhoto(usr, photo,out _ret);
        }
        public bool addPhoto(int usr, ViewPhotoDetail photo, out string msg)
        {
            try
            { 
                Post _post = new Post { UsuarioFK = usr, URL = photo.URL };
                _post.Metric = new Metric { Likes = photo.Metrics.likes, Shares = photo.Metrics.shares, Clicks = photo.Metrics.clicks };
                _ctx.Add(_post) ;
                msg = "Added new photo with sucess!";
                return _ctx.SaveChanges() != 0;
            }
            catch (Exception e)
            {
                msg = string.Format("Error during add new photo{0}{1}", Environment.NewLine, e.Message);
                return false;
            }
        }
        public bool addVideo(int usr, ViewVideoDetail video)
        { 
            string _ret;
            return addVideo(usr, video, out _ret);
        }
        public bool addVideo(int usr, ViewVideoDetail video, out string msg)
        {
            try {  
                Post _post = new Post { UsuarioFK = usr, URL = video.URL };
                _post.Metric = new Metric { Likes = video.Metrics.likes, completed_view = video.Metrics.completed_view, Clicks = video.Metrics.clicks };
                _ctx.Add(_post);
                msg = "Added new video with sucess!";
                return _ctx.SaveChanges() != 0;
            }
            catch (Exception e)
            {
                msg = string.Format("Error during add new video{0}{1}", Environment.NewLine, e.Message);
                return false;
            }
        } 
    }
}
