﻿using Data.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Domain.Models
{
    public class Post : BaseEntity
    {
        public int PostId { get; set; }
        public int UsuarioFK { get; set; }
        [ForeignKey("UsuarioFK")]
        public virtual User User { get; set; }
        public string URL { get; set; }
        public int MetricFK { get; set; }
        [ForeignKey("MetricFK")]
        public virtual Metric Metric { get; set; }
        public PostType Type { get; set; }
    }
}
