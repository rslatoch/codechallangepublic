﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Domain.Models
{
    public class User : BaseEntity
    {
        public int UserId { get; set; } 
        public string Name { get; set; }
        public DateTime Birthday { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Invalid email")]
        public string Email { get; set; } 
        public string Notes { get; set; }
        public List<Post> Posts { get; set; }
        public bool Illuminati { get; set; }
    }
}
