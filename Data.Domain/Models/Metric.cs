﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Domain.Models
{
    public class Metric : BaseEntity
    {
        public int MetricId { get; set; }
        public int PostFK { get; set; }
        [ForeignKey("PostFK")]
        public virtual Post Post { get; set; } 
        public int Clicks { get; set; }
        public int Likes { get; set; }
        public int Shares { get; set; } 
        public int completed_view { get; set; }

    }
}
