﻿using System; 
namespace CMD.Domain
{
    class Program
    {
        static void Main(string[] args)
        {
            string _return;
            Console.WriteLine(String.Format("Matrix validation, {0}{0}" +
                "Enter the values of the matrix, {0}" +
                "line by line, from left to right {0}" +
                "whith a comma (,) separator {0}{0}" +
                "Exemple: {0}" +
                " | 0 1 2 | {0}" +
                " | 3 4 5 | {0}" +
                " | 6 7 8 | {0}{0}" +
                "equals 0, 1, 2, 3, 4, 5, 6, 7, 8 {0}", System.Environment.NewLine));
            do
            {
                Console.WriteLine(String.Format("{0}now enter the values:{0} ", System.Environment.NewLine));
                Security.Validation.Matrix.Validate(Console.ReadLine(),out _return); 
                Console.WriteLine(String.Format("{0}{1}{0}{0}", System.Environment.NewLine,_return));
                Console.WriteLine("Do you whant validate another matrix? (Y/N)");
            } while (Console.ReadLine().ToUpper().Equals("Y"));
             
         }
    }
}
