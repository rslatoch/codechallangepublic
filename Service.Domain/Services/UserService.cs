﻿using Data.Domain.Enums;
using Data.Domain.Models;
using Data.Domain.Supervisor;
using Data.Domain.Views;
using Data.Domain.Views.UserReturn; 
using System;
using System.Collections.Generic;
using System.Text; 

namespace Service.Domain.Services
{
    public class UserService
    {
        private AppDbContext _ctx;
        private UserSupervisor _usr;  
        public UserService(AppDbContext appDb)
        {
            _ctx = appDb;
            _usr = new UserSupervisor(_ctx);
        }

        public ViewPeople GetList(ref ViewPagination pag)
        {
            try
            {
                List<User> _lst = _usr.toList(ref pag);
                foreach (User _tust in _lst)
                    _usr.AddView(_tust.UserId); 
                return FormatReturnPeople(_lst);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public ViewPerson GetById(int userId)
        {
            try
            {
                User tmpUser = _usr.Find(userId);
                if (!(tmpUser is null))
                    _usr.AddView(userId);
                return FormatReturnPerson(tmpUser);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public User GetByIdWhithoutFormat(int userId)
        {
            try
            {
                User tmpUser = _usr.Find(userId);
                if (!(tmpUser is null))
                    _usr.AddView(userId);
                foreach (Post pst in tmpUser.Posts)
                {
                    pst.User = null;
                    pst.Metric.Post = null;
                }
                return tmpUser;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public bool Remove(int userId)
        {
            try
            {  
                return _usr.Remove(userId);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public ViewPeople GetByName(string userName, string email="")
        {
            try
            {
                List<User> _lst = _usr.Find(userName, email);
                foreach (User _tust in _lst)
                    _usr.AddView(_tust.UserId);
                return FormatReturnPeople(_lst);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public int Add(ViewUserAdd user)
        {
            try
            {
                return _usr.Add(user);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public int Add(User user)
        {
            try
            {
                return _usr.Add(user);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public List<ViewReturnTopFive> TopFive(int month, int year)
        {
            try
            {
                return (_usr.topFive(month, year));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public List<ViewReturnTopFive> TopFive()
        {
            try
            {
                return TopFive(DateTime.Now.Month,DateTime.Now.Year);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public void AlterFlagIlluminati(int userid, bool iluminatti)
        {
            try
            {
                _usr.AlterFlagIlluminati(userid,iluminatti);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public void SendInvite(int userId,string email,string name)
        {
            EmailService.SendInvite(email, _usr.Find(userId).Name, name);
        }
        public ViewPeople FormatReturnPeople(List<User> users)
        {
            ViewPeople _ret = new ViewPeople();
            _ret.People = new List<ViewPerson>();
            foreach (User user in users)
            { 
                _ret.People.Add(FormatReturnPerson(user));
            } 
            return _ret;
        }
        public ViewPerson FormatReturnPerson(User user)
        {
            if (user is null)
                return null;
            ViewPerson _prsn = new ViewPerson { DateOfBirth = user.Birthday, Name = user.Name, Notes = user.Notes, summary_metrics = new ViewSummaryMetrics() };
            foreach (Post post in user.Posts)
            {
                if (_prsn.summary_metrics.posts is null)
                    _prsn.summary_metrics.posts = new List<Object>();
                switch (post.Type)
                {
                    case PostType.Photo:
                        ViewPhoto _pht = new ViewPhoto();
                        _pht.Photo = new ViewPhotoDetail { URL = post.URL };
                        _pht.Photo.Metrics = new ViewPhotoMetrics { clicks = post.Metric.Clicks, likes = post.Metric.Likes, shares = post.Metric.Shares };
                        _prsn.summary_metrics.posts.Add(_pht);
                        break;
                    case PostType.Video:
                        ViewVideo _vd = new ViewVideo();
                        _vd.Video = new ViewVideoDetail { URL = post.URL };
                        _vd.Video.Metrics = new ViewVideoMetrics { clicks = post.Metric.Clicks, likes = post.Metric.Likes, completed_view = post.Metric.completed_view };
                        _prsn.summary_metrics.posts.Add(_vd);
                        break;
                    default:
                        break;
                }
            }
            return _prsn;
        }
        public bool Exists(int userid)
        {
            return !(_usr.Find(userid) is null);
        }
    }
}
