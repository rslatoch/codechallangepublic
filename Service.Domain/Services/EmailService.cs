using System;
using System.Collections.Generic;
using System.Text;
using System.IO; 
using System.Net.Mail;
using Service.Domain.Interface;
using System.Net;

namespace Service.Domain.Services
{
    public class EmailService  
    {  
        public static void Send(string To, string Subject, string Mensage)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("xxxx@gmail.com", "xxxx");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("xxxx@gmail.com");
            mailMessage.To.Add(To);
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = Mensage;
            mailMessage.Subject = Subject;
            client.Send(mailMessage);

        } 
        public static void SendInvite(string To, string oldUsr, string newUsr)
        {
            StreamReader reader = File.OpenText(@"./Resources/Templates/EmailInvite.html");
            Send(To, "Invite - Interview", reader.ReadToEnd().Replace("#{NEWUSER}#", newUsr).Replace("#{OLDUSER}#",oldUsr));
        }

    }
}
