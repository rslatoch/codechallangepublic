﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Domain.Interface
{
    public interface IEmail
    {
        void Send(string To, string Subject, string Mensage);
        void SendInvite(string To, string oldUsr,string newUsr);
    }
}
